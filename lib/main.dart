import 'package:flutter/material.dart';

void main() {
  runApp(new MaterialApp(home: new application()));
}

class application extends StatefulWidget {
  @override
  _applicationState createState() => _applicationState();
}

class _applicationState extends State<application> with SingleTickerProviderStateMixin {

  String mtext = '';

  TabController _tcontrol;

  @override
  void initState() {
    _tcontrol = new TabController(length: 3, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.green,
        title: new Text('AppBar Widget'),
        centerTitle: true,
        elevation: 30.0,
        toolbarOpacity: 0.5,
        leading: new Icon(Icons.menu),
        actions: <Widget>[
          new IconButton(icon: new Icon(Icons.arrow_forward),
              onPressed:
                  () {setState(() {mtext = 'Arrow Button';
          });}),
          new IconButton(icon: new Icon(Icons.close),
              onPressed:
                  () {setState(() {mtext = 'Close Icon Button';
          });})
        ],
        bottom: new TabBar(
          controller: _tcontrol,
        tabs: <Widget>[
          new Tab(icon: new Icon(Icons.home),),
          new Tab(icon: new Icon(Icons.supervisor_account),),
          new Tab(icon: new Icon(Icons.close),),
        ],),
      ),
      body: new TabBarView(
        controller: _tcontrol,
        children: <Widget>[
          new Center(child: new Text('body 1'),),
          new Center(child: new Text('user acct'),),
          new Center(child: new Text('other'),),
        ],
      ),
        bottomNavigationBar: new Material(
        color: Colors.green,
          child: new TabBar(
            controller: _tcontrol,
            tabs: <Widget>[
              new Tab(icon: new Icon(Icons.home),),
              new Tab(icon: new Icon(Icons.supervisor_account),),
              new Tab(icon: new Icon(Icons.close),),
            ],)
        )
    );
  }
}
